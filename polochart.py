from enum import Enum
from random import randrange
from datetime import date
import matplotlib.pyplot as plt
import requests

url = 'https://poloniex.com/public?command=returnChartData&currencyPair=BTC_{coin}&start={start}&end={end}&period={period}'
coin = 'ETH'
start = 0
period = 86400
end = 9999999999
# 300, 900, 1800, 7200, 14400, and 86400

class Period(Enum):
    five = 300
    fifteen = 900
    thirty = 1800

response = requests.get(url.format(coin=coin, start=start, period=period, end=end))

print(response.json()[1])

quotevolume = []
volume = []
dates = []
close = []
open_ = []
low = []
high = []

for line in response.json():
    quotevolume.append(line['quoteVolume'])
    volume.append(line['volume'])
    close.append(line['close'])
    dates.append(date.fromtimestamp(line['date']))
    open_.append(line['open'])
    low.append(line['low'])
    high.append(line['high'])

def average(days, collection):
    average = []
    average = [0 for x in range(days)]
    for i in range(len(collection)-days):
        average.append(sum(collection[i:i+days])/days)
    return average

def data_plot(name, collection):
    avrg = average(30,collection)
    avrg2 = average(60,collection)
    avrg3 = average(90,collection)
    avrg4 = average(14,collection)

    print(len(avrg), len(close))

    fig = plt.figure()
    fig.subplots_adjust(right=0.95, bottom=0.50, left=0.10, top=0.95)
    plt.plot(dates, collection, label=name, linewidth = 2)
    plt.plot(dates, avrg, label='30 day average')
    plt.plot(dates, avrg2, label='60 day average')
    plt.plot(dates, avrg3, label='90 day average')
    plt.plot(dates, avrg4, label='14 day average')
    plt.legend()
    plt.xlabel('date')
    plt.ylabel('volume')
    fig.subplots_adjust(bottom=0.45)
    plt.show()

data_plot('volume', volume)