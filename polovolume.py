import json
import requests
from time import sleep, clock

url = "https://poloniex.com/public?command=return24hVolume"

while True:
    response = requests.get(url)
    json_data = response.json()

    keys = [key for key in json_data.keys() if key.startswith('BTC')]

    new_dict = {}
    for ticker in keys:
        # print(ticker)
        # print(json_data[ticker]['BTC'])
        # print()
        new_dict[ticker] = float(json_data[ticker]['BTC'])

    for x in sorted(zip(new_dict.values(), new_dict.keys()), reverse = False):
        print(x)
    print()
    print(clock())
    print()
    sleep(30)
